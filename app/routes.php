<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

use ElephantIO\Client as Elephant;



Route::get('/', function()
{
	return View::make('chat');
});

Route::get('test', function()
{
	$elephant = new Elephant('http://localhost:8083', 'socket.io', 1, false, true, true);
	$elephant->init();

	$elephant->send(
	    Elephant::TYPE_EVENT,
	    null,
	    null,
	    json_encode(array('name' => 'foo', 'args' => 'bar'))
	);

	$elephant->close();

	echo 'tryin to send `bar` to the event `foo`';
});

